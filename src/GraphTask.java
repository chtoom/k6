import java.util.*;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */

    /**
     * Actual main method to run examples and everything.
     */


    // TODO!!! add javadoc relevant to your problem
    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;
        // You can add more fields, if needed

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        public void setInfo(int info) {
            this.info = info;
        }

        public int getInfo() {
            return this.info;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        // TODO!!! Your Vertex methods here!
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int info = 0;
        // You can add more fields, if needed

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        // TODO!!! Your Arc methods here!
    }


    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;
        // You can add more fields, if needed

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        //Allikas: https://examples.javacodegeeks.com/core-java/util/stringtokenizer/reverse-string-
        //with-stringtokenizer/
        /*
        * Prints out the shortest path from given starting point to the endpoint.
        * @param: Graph class object
        * @param: int starting vertex
        * @param: int endpoint
        * */
        public void printPath (Graph graph, int startVertex, int w) {
            int[] prev = findPathBFS(graph, startVertex);
            StringBuilder path = new StringBuilder();
            int currentV = w;

            try {
                while (prev[currentV-1] != -1) {
                    path.append(currentV);
                    path.append(" ");
                    currentV = prev[currentV-1];
                }
                path.append(startVertex);
                String bwPath = path.toString();
                String finalPath = "";
                StringTokenizer st = new StringTokenizer(bwPath);
                while (st.hasMoreTokens()) {
                    finalPath = st.nextToken() + "->" + finalPath;
                }
                System.out.println("Path from " + startVertex + " to " + w + " is: " + finalPath.substring(0, finalPath.length()-2));
            } catch (Exception e) {
                throw new RuntimeException("Not path found from " + startVertex + " to " + w);
            }
        }
    }



        /*
        * Using the graph adjacency matrix representation, creates a HashMap object where the key is the int value of the
        * vertex and value is a LinkedList object of all adjacent vertices relative to the vertex.
        * @param: int[][] adjacency matrix
        * @return: HashMap<Integer, LinkedList<Integer>> adjacency list.
        * */
        public HashMap<Integer, LinkedList<Integer>> createAdjMap(int[][] matrix) {
            int length = matrix.length;
            HashMap<Integer, LinkedList<Integer>> vertixWAdjacents = new HashMap<>();
            for (int i = 0; i < length; i++) {
                LinkedList<Integer> adjacentVertices = new LinkedList<>();
                for (int j = 0; j < length; j++) {
                    if (matrix[i][j] == 1) {
                        adjacentVertices.add(j + 1);
                    }
                }
                vertixWAdjacents.put(i + 1, adjacentVertices);
            }
            System.out.println("Graph: " + vertixWAdjacents);
            return vertixWAdjacents;
        }



        /*
        * Performs breadth-first search on a graph. Returns an array of integers which size corresponds to the total number
        * of vertices in the given graph. In the array each position signifies the vertex that was traversed before
        * traversing the current vertex. E.g the vertex 6 was traversed before the vertex 5. So in the array at position
        * 5 the value of the array would be 6.
        * @param: graph class instance, on what the BFS algorithm would be performed on.
        * @param: int start point. Where the BFS algorithm starts
        * @return: array of integers.
        *
        * */
        //Allikas: https://crab.rutgers.edu/~guyk/BFS.pdf, https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
        public int[] findPathBFS(Graph graph, int startVertex) {
            // Create adjacency matrix for given graph.
            // Create a dictionary, where key is vertix id and value is a list of adjacent vertices.
            int[][] adjMatrix = graph.createAdjMatrix();
            HashMap<Integer, LinkedList<Integer>> adjMap = createAdjMap(adjMatrix);
            StringBuilder path = new StringBuilder();

            // Since HashSet only stores each value once, it's ideal for marking up vertices that have already been visited.
            HashSet<Integer> visited = new HashSet<>();
            LinkedList<Integer> queue = new LinkedList<>();

            // Claim that starting point has been visited and add it to the queue.
            visited.add(startVertex);
            queue.add(startVertex);


            int[] pred = new int[adjMatrix.length];


            Arrays.fill(pred, -1);

            while (queue.size() != 0) {
                // FIFO realisation.
                Integer s = queue.poll();
                path.append(s);
                path.append("->");

                for (Integer next : adjMap.get(s)) {
                    if (!visited.contains(next)) {
                        visited.add(next);
                        queue.add(next);
                        // Stores the value of previous vertex.
                        pred[next-1] = s;
                    }
                }
            }
            return pred;
        }



    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    public void run() {
        // Uue Graph klassi objekti loomine.
        long start = System.currentTimeMillis();
        Graph g = new Graph("G");
        // Soovitud parameetritega sidusa suunamata kaaludeta graafi loomine.
        g.createRandomSimpleGraph(2001, 2019);
        // Kasutaja määrab algtipu ja lõpptipu, mille vahelise teekonna algoritm peab leidma. Sisendiks peab andma ka loodud graafi objekti.
        g.printPath(g, 1, 7);
        long stop  = System.currentTimeMillis();
        //Programm tagastab lühima tee alpunktist lõpppunkti.
        System.out.println("Time elapsed: " + (stop - start) + " ms");
    }
} 

